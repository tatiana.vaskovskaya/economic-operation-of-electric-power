import pandas as pd

GEN_COLUMNS = ['gen_bus', 'pg', 'qg', 'qmax', 'qmin', 'vg', 'mbase', 'gen_status', 'pmax', 'pmin',
               'pc1', 'pc2', 'qc1min', 'qc1max', 'qc2min', 'qc2max', 'ramp_agc', 'ramp_10', 'ramp_30', 'ramp_q',
               'apf', 'mu_pmax', 'mu_pmin', 'mu_qmax', 'mu_qmin']

BUS_COLUMNS = ['bus_i', 'bus_type', 'pd', 'qd', 'gs', 'bs', 'bus_area', 'vm', 'va', 'base_kv', 'zone',
               'vmax', 'vmin', 'lam_p', 'lam_q', 'mu_vmax', 'mu_vmin']

BRANCH_COLUMNS = ['f_bus', 't_bus', 'br_r', 'br_x', 'br_b', 'rate_a', 'rate_b', 'rate_c', 'tap', 'shift',
                  'br_status', 'angmin', 'angmax', 'pf', 'qf', 'pt', 'qt', 'mu_sf', 'mu_st',
                  'mu_angmin', 'mu_angmax']

COST_COLUMNS = ['model', 'startup', 'shutdown', 'ncost', 'col1', 'col2', 'col3', 'col4', 'col5', 'col6']

class ppcDataframe:

    def __init__(self):
        self.bus = None
        self.gen = None
        self.branch = None
        self.gencost = None
        self.version = '2'
        self.baseMVA = 100.0
        self.bus_type = BusType()
        self.gencost_model = GenCostModel()


class BusType:
    def __init__(self):
        self.pq = 1
        self.pv = 2
        self.ref = 3
        self.none = 4


class GenCostModel:
    def __init__(self):
        self.pw_linear = 1
        self.polynomial = 2


def ppc_to_dataframes(ppc):
    ppc_df = ppcDataframe()

    def apply_dataframe(table_name, columns):
        cols_count = ppc[table_name].shape[1]
        return pd.DataFrame(data=ppc[table_name], columns=columns[:cols_count])

    ppc_df.bus = apply_dataframe(table_name='bus', columns=BUS_COLUMNS)
    ppc_df.bus[['bus_i', 'bus_type', 'bus_area', 'zone']] = \
        ppc_df.bus[['bus_i', 'bus_type', 'bus_area', 'zone']].astype(int)

    ppc_df.branch = apply_dataframe(table_name='branch', columns=BRANCH_COLUMNS)
    ppc_df.branch[['f_bus', 't_bus', 'br_status']] = ppc_df.branch[['f_bus', 't_bus', 'br_status']].astype(int)


    ppc_df.gen = apply_dataframe(table_name='gen', columns=GEN_COLUMNS)
    ppc_df.gen[['gen_bus', 'gen_status']] = ppc_df.gen[['gen_bus', 'gen_status']].astype(int)


    ppc_df.gencost = apply_dataframe(table_name='gencost', columns=COST_COLUMNS)
    ppc_df.gencost[['model', 'ncost']] = ppc_df.gencost[['model', 'ncost']].astype(int)

    if 'gen_names' in ppc:
        ppc_df.gen['name'] = ppc['gen_names']
        ppc_df.gencost['name'] = ppc['gen_names']

    return ppc_df


def dataframes_to_ppc(ppc_df: ppcDataframe):
    res = dict()
    if 'name' in ppc_df.gen.columns:
        res['gen_names'] = ppc_df.gen.name
        ppc_df.gen = ppc_df.gen.drop(columns='name')
        ppc_df.gencost = ppc_df.gencost.drop(columns='name')

    def apply_ppc(table_name, columns):
        df = getattr(ppc_df, table_name)
        cols_count = df.shape[1]
        return df[columns[:cols_count]].to_numpy()

    res['bus'] = apply_ppc('bus', BUS_COLUMNS)
    res['gen'] = apply_ppc('gen', GEN_COLUMNS)
    res['branch'] = apply_ppc('branch', BRANCH_COLUMNS)
    res['gencost'] = apply_ppc('gencost', COST_COLUMNS)
    res['version'] = '2'
    res['baseMVA'] = 100.0

    return res
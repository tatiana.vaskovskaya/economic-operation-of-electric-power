from datetime import date, timedelta, datetime
from itertools import product
import lxml.etree as et
import lxml.html as html
import xlrd
import zipfile
import os
import pandas as pd
import pickle
import time
from loadutil.returns import retry, safe
from loadutil.scrapping.common import Report, try_urlopen


# TODO use _cell_values for best performance

price_zones = [None, 'eur', 'sib']

with open(os.path.join(os.path.dirname(__file__), 'regions'), 'rb') as f:
    regions = pickle.load(f)

dir_map = {'Покупка': 1, 'Продажа': 2}

region_map = {
    region_name: i
    for i, region_name in zip(regions.index, regions.region_name)
}
region_pz = {
    region: pz
    for region, pz in zip(regions.index, regions.pz)
}

null = float('NaN')


class AtsReport(Report):
    REPORT_URL = 'https://www.atsenergo.ru/nreport'
    REPORTS_LIST_URL = 'https://www.atsenergo.ru/nreport?access=public&rname={report}&rdate={date:%Y%m%d}&region={pz}'
    _context = None
    pz = [1, 2]
    period = 'day'
    verbose = False

    def _get_reports(self, report_type: str, target_date: date, pz: int):
        if report_type is None or target_date is None or pz is None:
            raise ValueError('Все атрибуты должны быть заполнены')
        url = self.REPORTS_LIST_URL.format(report=report_type, date=target_date, pz=price_zones[pz])

        page = html.parse(try_urlopen(url)).getroot()
        url_list = [self.REPORT_URL + a.get('href') for a in page.xpath('.//a[contains(@href, "zip=1")]')]
        return url_list

    @safe(on_error=print)
    def __get_xls(self, url):
        content = try_urlopen(url)
        with zipfile.ZipFile(content) as zip:
            fn = zip.namelist()[0]
            self._context['filename'] = fn
            with zip.open(fn) as xls:
                return xlrd.open_workbook(file_contents=xls.read())

    def _read_xls(self, wb: xlrd.book.Book):
        raise NotImplementedError()

    def _download(self, urls):
        for url in urls:
            wb = self.__get_xls(url)
            self._read_xls(wb)

    def download(self, start_date, end_date=None):
        periods = self._periods(start_date, end_date)

        self.data = []
        for d, pz in product(periods, self.pz):
            if self.verbose:
                print(f'{self.report_type} - ЦЗ {pz} - {d:%Y-%m-%d}')
            self._context = {'date': d, 'pz': pz}
            self._download(url for url in self._get_reports(self.report_type, d, pz))
            time.sleep(5)

        if len(self.data):
            return pd.concat(self.data)  # type: pd.DataFrame
        else:
            return pd.DataFrame([])


class BranchReport(AtsReport):
    report_type = 'TS_PART_REP_LINE'
    pz = [1, ]  # данные для обеих ЦЗ дублируются

    def _read_xls(self, wb: xlrd.book.Book):
        data = list()
        for ws in wb.sheets():  # type: xlrd.sheet.Sheet
            hour_data = pd.DataFrame(
                data={
                    'hour': int(ws.name),
                    'node_from': ws.col_values(0, 5),
                    'node_to': ws.col_values(1, 5),
                    'branch_num': ws.col_values(2, 5),
                    'flow': ws.col_values(3, 5)
                }
            )
            data.append(hour_data)

        data = pd.concat(data)  # type: pd.DataFrame
        data['date'] = pd.to_datetime(self._context['date'])
        # в какой-то момент int превращаются в float
        data.node_from = data.node_from.astype(int)
        data.node_to = data.node_to.astype(int)
        data.branch_num = data.branch_num.astype(int)
        data.hour = data.hour.astype(int)

        self.data.append(data)


class NodePriceReport(AtsReport):
    report_type = 'big_nodes_prices_pub'
    pz = [1, ]  # данные для обеих ЦЗ дублируются

    def _read_xls(self, wb: xlrd.book.Book):
        data = list()
        for ws in wb.sheets():  # type: xlrd.sheet.Sheet
            hour_data = pd.DataFrame(
                data=ws._cell_values[3:],
                columns=[
                    'node_id',
                    'node_name',
                    'u',
                    'region_name',
                    'price',
                    'empty'
                ]
            )
            hour_data.drop(['empty', ], axis=1, inplace=True)
            hour_data['hour'] = int(ws.name)
            hour_data['region_id'] = hour_data.region_name.replace(region_map).astype(int)
            hour_data['price'] = pd.to_numeric(hour_data.price)
            data.append(hour_data)

        data = pd.concat(data)  # type: pd.DataFrame
        data['date'] = pd.to_datetime(self._context['date'])
        # в какой-то момент int превращаются в float
        data.node_id = data.node_id.astype(int)
        data.hour = data.hour.astype(int)

        self.data.append(data)


class DguVolumeReport(AtsReport):
    report_type = 'carana_sell_units'

    def _read_xls(self, wb: xlrd.book.Book):
        data = list()
        ws = wb.sheet_by_index(0)  # type: xlrd.sheet.Sheet
        d = datetime.strptime(ws.cell_value(2, 2), '%Y-%m-%d').date()
        if d >= date(2021, 2, 1):
            ncols = 7
            columns = ['dgu_id', 'dgu_name', 'hour', 'node_id', 'p_min_tech', 'p_min_techn', 'p_min', 'v_ppp',
                       'p_max', 'p_max_tech', 'p_max_techn']
        else:
            ncols = 5
            columns = ['dgu_id', 'dgu_name', 'hour', 'node_id', 'p_min_tech', 'p_min_techn', 'p_min', 'v_ppp',
                       'p_max']
        row = 7
        while ws.cell_value(row, 0) != '':
            dgu_id = int(ws.cell_value(row, 0))
            dgu_name = ws.cell_value(row, 1)
            node_id = int(ws.cell_value(row, 2))
            data_row = ws.row_values(row, 4, 4 + ncols * 24)
            for i in range(0, ncols * 24, ncols):
                data.append((dgu_id, dgu_name, i // ncols, node_id, *data_row[i:i+ncols]))
            row += 1

        data = pd.DataFrame(data, columns=columns)
        data['date'] = pd.to_datetime(self._context['date'])
        # в какой-то момент int превращаются в float
        data.node_id = data.node_id.astype(int)
        data.hour = data.hour.astype(int)
        data.dgu_id = data.dgu_id.astype(int)

        self.data.append(data)


class DemandOfferCurveReport(AtsReport):
    report_type = 'curve_demand_offer'

    def _read_xls(self, wb: xlrd.book.Book):
        data = list()
        for ws in wb.sheets():  # type: xlrd.sheet.Sheet
            p_list = ws.col_values(3, 8)
            if p_list[0] == "*":
                p_list[0] = 0

            hour_data = pd.DataFrame(
                data={
                    'hour': int(ws.name),
                    'price': p_list,
                    'volume': ws.col_values(4, 8)
                }
            )
            data.append(hour_data)

        data = pd.concat(data)  # type: pd.DataFrame
        data['date'] = pd.to_datetime(self._context['date'])
        data['pz'] = self._context['pz']
        # в какой-то момент int превращаются в float
        data.pz = data.pz.astype(int)
        data.hour = data.hour.astype(int)

        self.data.append(data)


class SectionReport(AtsReport):
    report_type = 'overflow_sechen_all_pub'

    def _read_xls(self, wb: xlrd.book.Book):
        data = list()
        for ws in wb.sheets():  # type: xlrd.sheet.Sheet
            hour_data = pd.DataFrame(
                data=ws._cell_values[3:],
                columns=[
                    'section_id',
                    'name',
                    'node1',
                    'node2',
                    'branch_name',
                    'p_min',
                    'p_max',
                    'v_ppp'
                ]
            )
            hour_data.replace(['', ' '], [null, null], inplace=True)
            t = hour_data.p_min.isnull() & hour_data.p_max.isnull()
            hour_data.drop(hour_data.index.values[t], inplace=True)
            hour_data.p_min = hour_data.p_min.astype(str).str.replace(',', '.').astype('float')
            hour_data.p_max = hour_data.p_max.astype(str).str.replace(',', '.').astype('float')
            hour_data.v_ppp = hour_data.v_ppp.astype(str).str.replace(',', '.').astype('float')
            hour_data['is_active'] = ((hour_data.p_min == hour_data.v_ppp) | (hour_data.p_max == hour_data.v_ppp)) * 1
            hour_data.drop(['name', 'node1', 'node2', 'branch_name', 'p_min', 'p_max', 'v_ppp'], axis=1, inplace=True)
            hour_data['hour'] = int(ws.name)
            data.append(hour_data)

        data = pd.concat(data)  # type: pd.DataFrame
        data['date'] = pd.to_datetime(self._context['date'])
        # в какой-то момент int превращаются в float
        data.section_id = data.section_id.astype(int)
        data.hour = data.hour.astype(int)
        data.is_active = data.is_active.astype(int)

        self.data.append(data)


class RegionReport(AtsReport):
    report_type = 'trade_region_spub'
    pz = [1, ]  # данные для обеих ЦЗ дублируются

    def _read_xls(self, wb: xlrd.book.Book):
        ws = wb.sheet_by_index(0)
        data = pd.DataFrame(
            data=ws._cell_values[6:],
            columns=[
                'region_name',
                'hour',
                'gen_ges',
                'gen_aes',
                'gen_tes',
                'gen_ses',
                'gen_ves',
                'gen_other',
                'pmin_tech_ges',
                'pmin_tech_aes',
                'pmin_tech_tes',
                'pmin_tech_ses',
                'pmin_tech_ves',
                'pmin_tech_other',
                'pmin_ges',
                'pmin_aes',
                'pmin_tes',
                'pmin_ses',
                'pmin_ves',
                'pmin_other',
                'pmax_ges',
                'pmax_aes',
                'pmax_tes',
                'pmax_ses',
                'pmax_ves',
                'pmax_other',
                'con',
                'exp',
                'imp',
                'price_con',
                'price_gen',
                'con_total'
            ]
        )
        data.replace('', null, inplace=True)
        data['region_id'] = data.region_name.replace(region_map).astype(int)
        data['hour'] = data.hour.astype(int)
        data['date'] = pd.to_datetime(self._context['date'])
        self.data.append(data)


class DRReport(Report):
    url_tmpl = 'https://www.atsenergo.ru/market/stats.xml?type=dr&date={:%Y%m%d}'

    def _download(self, url):
        xml_data = try_urlopen(url)
        root = et.parse(xml_data)
        row = root.findall('row')[-1]
        is_dr, is_agg_test = row.findall('col')[4:6]
        is_dr = is_dr.text != '0'
        is_agg_test = is_agg_test.text != '0'
        return is_dr, is_agg_test

    def download(self, start_date, end_date=None):
        periods = self._periods(start_date, end_date)

        self.data = []
        for d in periods:
            self.data.append((d, *self._download(self.url_tmpl.format(d))))

        return pd.DataFrame(self.data, columns=['date', 'is_dr', 'is_agg_test'])


class RegionFactReport(AtsReport):
    """Отчет по фактическому потреблению в регионах"""
    report_type = 'fact_region'
    pz = [1, ]  # данные для обоих ЦЗ дублируются
    period = 'month'

    def _read_xls(self, wb: xlrd.book.Book):
        ws = wb.sheet_by_index(0)
        data = pd.DataFrame(
            data=ws._cell_values[7:],
            columns=['date', 'hour', 'fact']
        )
        data['region_id'] = region_map[ws._cell_values[1][1]]
        data['date'] = pd.to_datetime(data['date'])
        data['hour'] = data.hour.astype(int) - 1
        self.data.append(data)


class RegionTotalReport(Report):
    """Составление отчета по всем доступным данным в разрезе региона.
    Комбинация отчетов:
        Отчет о торгах по субъектам РФ ЕЭС
        Отчёт о перетоках мощности по контролируемым сечениям
    """

    def download(self, start_date, end_date=None):
        days = self._periods(start_date, end_date)
        region_report = RegionReport()
        section_report = SectionReport()
        blk_report = SOBlockStationsReport()

        total_report = []
        for day in days:
            region_data = region_report.download(day)
            section_data = section_report.download(day)
            blck_stan_data = blk_report.download(day)

            # в данных по сечениям есть повторы
            section_data = section_data.groupby(['date', 'hour', 'section_id'])['is_active'].max().reset_index()
            active_sections = section_data.groupby('section_id')['is_active'].sum()
            active_sections = active_sections[active_sections > 0].index
            section_data = section_data[section_data.section_id.isin(active_sections)]
            section_data = section_data.set_index(['date', 'hour', 'section_id']).unstack(level='section_id')
            section_data.columns = ['{}_{}'.format(*c) for c in section_data.columns]

            total_report.append(pd.merge(
                pd.merge(
                    region_data,
                    blck_stan_data,
                    on=['date', 'hour', 'region_id'],
                    how='left'
                ),
                section_data.reset_index(),
                on=['date', 'hour']
            ))
        return pd.concat(total_report).fillna(0.0)


class RegionFlowReport(AtsReport):
    report_type = 'overflow_region_spub'

    def _read_xls(self, wb: xlrd.book.Book):
        ws = wb.sheet_by_index(0)
        data = pd.DataFrame(
            data=ws._cell_values[6:],
            columns=[
                'region1_id',
                'region2_id',
                'hour',
                'flow'
            ]
        )
        data.replace('', null, inplace=True)
        data['region1_id'] = data.region1_id.replace(region_map).astype(int)
        data['region2_id'] = data.region2_id.replace(region_map).astype(int)
        data['hour'] = data.hour.astype(int)
        data['date'] = pd.to_datetime(self._context['date'])
        self.data.append(data)


class SoReport(Report):
    REPORT_URL = 'http://br.so-ups.ru/webapi/Public/Export/Csv/{report}.aspx'
    url_tmpl = None
    period = 'day'
    verbose = False

    def _read_csv(self, csv):
        raise NotImplementedError()

    def _download(self, url):
        self.data.append(self._read_csv(try_urlopen(url)))

    def download(self, start_date, end_date=None):
        self.data = []
        for d in self._periods(start_date, end_date):
            if self.verbose:
                print(f'{self.report_type} - {d:%Y-%m-%d}')
            url = self.url_tmpl.format(report=self.report_type, date=d)
            self._download(url)

        return pd.concat(self.data)


class SOGenConsumReport(SoReport):
    report_type = 'GenConsum'
    url_tmpl = SoReport.REPORT_URL + '?startDate={date:%d.%m.%Y}&endDate={date:%d.%m.%Y}&' \
                                     'territoriesIds=:530000,:550000,:600000,:610000,:630000,:840000&notCheckedColumnsNames='

    def _read_csv(self, csv):
        data = pd.read_csv(csv, sep=';', decimal=',')
        data.rename(inplace=True, columns={
            'INTERVAL': 'hour',
            'M_DATE': 'date',
            'POWER_SYS_ID': 'oes_id',
            'E_USE_FACT': 'vol_con_fact',
            'E_USE_PLAN': 'vol_con_plan',
            'GEN_FACT': 'vol_gen_fact',
            'GEN_PLAN': 'vol_gen_plan'
        })
        data.drop('PRICE_ZONE_ID', axis=1, inplace=True)
        data.replace(to_replace={'oes': {530000: 1, 550000: 2, 600000: 3, 610000: 4, 630000: 5, 840000: 7}},
                     inplace=True)
        return data


class SOForecastConsumSubReport(SoReport):
    report_type = 'ForecastConsumSubRf'
    url_tmpl = SoReport.REPORT_URL + '?date={date}'

    def _read_csv(self, csv):
        data = pd.read_csv(csv, sep=';', decimal=',')
        data.rename(inplace=True, columns={
            'sub_rf_id': 'region_id',
            'cons_value': 'vol_con_plan'
        })
        data.date = pd.to_datetime(data.date, format='%d.%m.%Y 0:00:00')
        return data


class SOBlockStationsReport(SoReport):
    report_type = 'PowerESPPByRegions'
    url_tmpl = SoReport.REPORT_URL + '?date={date}'

    def _read_csv(self, csv):
        data = pd.read_csv(csv, sep=';', decimal=',')
        data.rename(inplace=True, columns={
            'sub_rf_id': 'region_id',
            'Pbst': 'vol_gen_blockstan'
        })
        data.date = pd.to_datetime(data.date, format='%d.%m.%Y 0:00:00')
        return data


class SOUCGenReport(SoReport):
    report_type = 'GenEquipOptions'
    url_tmpl = SoReport.REPORT_URL + '?date={date}&territoriesIds=1:null,2:null&notCheckedColumnsNames=&zone=1'

    def _read_csv(self, csv):
        data = pd.read_csv(csv, sep=';', decimal=',')
        data.date = pd.to_datetime(data.date, format='%d.%m.%Y 0:00:00')
        return data


class SOUCSupply(SoReport):
    report_type = 'ProposalEnergy'
    url_tmpl = SoReport.REPORT_URL + '?dates={date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date},{date}' +\
                                     '&territoriesIds=1,2&checkedParameterIds=vsvgo&zone=1&hours=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23&isStartsProposal=false&isSubRfProposal=false'

    def _read_csv(self, csv):
        data = pd.read_csv(csv, sep=';', decimal=',', encoding='cp1251')
        data.date = pd.to_datetime(data.date, format='%d.%m.%Y 0:00:00')
        p_max = data.groupby(['date', 'hour', 'price_zone_id'])['price'].idxmax().values
        data = data.iloc[p_max, :]
        data.drop(['Type', 'price'], axis=1, inplace=True)
        data.rename(columns={'value': 'pmax_uc'}, inplace=True)
        return data


class SOUCConReport(SoReport):
    report_type = 'ForecastConsum'
    url_tmpl = SoReport.REPORT_URL + '?date={date}&territoriesIds=1:null,2:null&' \
                                     'notCheckedColumnsNames=&zone=1&isConsuming=true'

    def _read_csv(self, csv):
        data = pd.read_csv(csv, sep=';', decimal=',')
        data.date = pd.to_datetime(data.date, format='%d.%m.%Y 0:00:00')
        return data


class SOUCIEReport(SoReport):
    report_type = 'BalanceEI'
    url_tmpl = SoReport.REPORT_URL + '?date={date}&priceZonesIds=1,2&zone=1'

    def _read_csv(self, csv):
        data = pd.read_csv(csv, sep=';', decimal=',')
        data.date = pd.to_datetime(data.date, format='%d.%m.%Y 0:00:00')
        return data


class SOUCFlowReport(SoReport):
    report_type = 'FlowPZ'
    url_tmpl = SoReport.REPORT_URL + '?date={date}&zone=1'

    def _read_csv(self, csv):
        data = pd.read_csv(csv, sep=';', decimal=',')
        data.date = pd.to_datetime(data.date, format='%d.%m.%Y 0:00:00')
        return data


class SOPZTotalReport(Report):
    verbose = False
    report_type = 'PZTotalReport'
    reports = [
        SOBlockStationsReport,
        SOUCGenReport,
        SOUCConReport,
        SOUCIEReport,
        SOUCFlowReport,
        SOUCSupply
    ]

    def download(self, start_date, end_date=None):
        days = self._periods(start_date, end_date)

        self.data = []
        for d in days:
            if self.verbose:
                print(f'{self.report_type} - {d:%Y-%m-%d}')
            self._download(d)

        return pd.concat(self.data)

    @retry(max_tries=5, on_error=print)
    def _download(self, day):
        data = {
            r.report_type: r().download(day)
            for r in self.reports
        }
        for name, df in data.items():
            if 'region_id' in df:
                df['price_zone_id'] = df.region_id.replace(region_pz)
                df = df.groupby(['date', 'hour', 'price_zone_id']).sum()
                df.drop('region_id', axis=1, inplace=True)
            elif 'price_zone_id' in df:
                df = df.set_index(['date', 'hour', 'price_zone_id'])
            else:
                df1 = df.copy()
                df1['price_zone_id'] = 1
                df1['flow'] *= -1

                df['price_zone_id'] = 2
                df = pd.concat((df1, df)).set_index(['date', 'hour', 'price_zone_id'])

            if 'power_sys_id' in df:
                df.drop('power_sys_id', axis=1, inplace=True)
            data[name] = df

        data = pd.concat(data.values(), axis=1)
        self.data.append(data.reset_index())


class SOTemperatureReport(Report):
    report_type = 'temperature'
    url_tmpl = 'http://so-cdu.ru/index.php?id=ees_temperature&no_cache=1&' \
               'tx_ms1cdu_pi1[kpo]={oes}&tx_ms1cdu_pi1[dt]={date:%d.%m.%Y}&tx_ms1cdu_pi1[format]=csv'

    kpo_oes = {
        1: '630000',
        2: '600000',
        3: '550000',
        4: '840000',
        5: '530000',
        10: '610000',
        11: '540000',
        0: '1019'
    }

    def _read_csv(self, csv, oes):
        data = pd.read_csv(csv, sep=';', names=['date', 'ips_temp'], skiprows=1)
        data['date'] = pd.to_datetime(data.date, format='%d-%m-%Y')
        data['ips_pk_fk'] = oes
        self.data.append(data)

    def download(self, start_date, end_date=None):
        if end_date is None:
            end_date = start_date
        the_date = end_date

        self.data = []
        # идем с конца в начало
        while the_date >= start_date:
            for oes, kpo in self.kpo_oes.items():
                url = self.url_tmpl.format(oes=kpo, date=the_date)
                self._read_csv(try_urlopen(url), oes)

            # сайт выдает инфу ровно за 31 день, переходим к следующей порции
            the_date -= timedelta(31)

        return pd.concat(self.data)

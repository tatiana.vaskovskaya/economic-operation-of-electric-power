from datetime import timedelta
from dateutil.relativedelta import relativedelta
import warnings
import requests
import urllib3
from loadutil.returns import retry
import io

warnings.filterwarnings("ignore", category=urllib3.exceptions.InsecureRequestWarning)
session = requests.Session()
# отключим проверку SSL сертификата для сайтов
session.verify = False


@retry(max_tries=5, on_error=print)
def try_urlopen(url: str):
    page = session.get(url, timeout=5)
    return io.BytesIO(page.content)


class Report:
    report_type = None
    data = None
    period = 'day'

    def _periods(self, start_date, end_date=None):
        if end_date is None:
            return [start_date, ]
        elif self.period == 'day':
            day_count = (end_date - start_date).days + 1
            return [start_date + timedelta(n) for n in range(day_count)]
        elif self.period == 'month':
            if start_date.day != 1:
                raise ValueError(f'Incorrect date: {start_date}')
            rd = relativedelta(end_date, start_date)
            month_count = rd.years * 12 + rd.months + 1
            return [start_date + relativedelta(months=n) for n in range(month_count)]
        elif self.period == 'year':
            rd = relativedelta(end_date, start_date)
            year_count = rd.years + 1
            if start_date.day != 1 or start_date.month != 1:
                raise ValueError(f'Incorrect date: {start_date}')
            return [start_date + relativedelta(years=n) for n in range(year_count)]
        else:
            raise RuntimeError(f'Incorrect period: {self.period}')

    # TODO make download common and override the _download method
    def download(self, *args, **kwargs):
        raise NotImplementedError()

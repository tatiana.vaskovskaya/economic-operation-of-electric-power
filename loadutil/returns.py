import time

DEFAULT_ERROR = object()


def safe(default=DEFAULT_ERROR, on_error=None):
    """
    Inspired by Dry Python https://github.com/dry-python/returns
    default - функция (фабрика объектов). Т.к. объект должен создаваться для каждого вызова функции,
              а не быть глобальным.
    on_error - функция с единственным аргументом - объектом ошибки
    """

    def decorator(function):
        def wrapped(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except Exception as exc:
                if on_error is not None:
                    on_error(exc)
                if default is DEFAULT_ERROR:
                    return exc
                else:
                    return default()
        return wrapped
    return decorator


def retry(max_tries=2, wait_for=2, on_error=None):
    def decorator(function):
        def wrapped(*args, **kwargs):
            n = max_tries
            while True:
                try:
                    return function(*args, **kwargs)
                except Exception as exc:
                    if on_error is not None:
                        on_error(exc)
                    n -= 1
                    if n == 0:
                        return exc
                    else:
                        time.sleep(wait_for)

        return wrapped

    return decorator

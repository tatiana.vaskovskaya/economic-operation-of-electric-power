from pypower.api import case30, runpf, runopf, ppoption, ppc_to_dataframes, dataframes_to_ppc
import numpy as np

# Загрузка ЭЭС с 30 узлами
ppc = case30()

# Настройки параметров оптимизации
ppopt = ppoption(OPF_FLOW_LIM=1  # active power flow (limit in MW)
                 )

# Запуск расчета установившегося режима ЭЭС
ppc, success = runpf(casedata=ppc, ppopt=ppopt)

# Перевод pypower case из словаря таблиц numpy в набор dataframes
ppc_df = ppc_to_dataframes(ppc)

# Формируем заявки в линейном виде
ppc_df.gencost['model'] = ppc_df.gencost_model.polynomial
ppc_df.gencost['ncost'] = 2
ppc_df.gencost.drop(columns='col3', inplace=True)
ppc_df.gencost['col1'] = np.arange(1000., 1600., 100.)
ppc_df.gencost['col2'] = 0.

# Ослабляем сетевые ограничения
ppc_df.branch['rate_a'] *= 1.5

# Объявляем генератор, с которым будем работать
ges = 3

# Объявляем цену генератора
ppc_df.gencost.loc[ges, 'col1'] = 0.01

# Устанавливаем свои параметры
ppc_df.gen.loc[ges, ['pmax', 'pmin']] = 30.0 # загрузка генератора
ppc_df.bus[['pd', 'qd']] *= 1.4 # коэффициент изменения нагрузки в зависимости от времени суток

# Возвращаем набор dataframes в словарь pypower case
ppc = dataframes_to_ppc(ppc_df)

# Оптимизируем
solved_ppc = runopf(casedata=ppc, ppopt=ppopt)

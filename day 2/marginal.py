import loadutil.scrapping.ats_so as ats_so
from datetime import date
import pandas as pd

date_to = date.today()
date_from = date.today()

# Загрузка данных по РГЕ
r = ats_so.DguVolumeReport()
r.verbose = True
dgu_data = r.download(date_from, date_to)
dgu_data['date'] = dgu_data.date.dt.date

# Загрузка данных по узлам
r = ats_so.NodePriceReport()
r.verbose = True
node_data = r.download(date_from, date_to)
node_data['date'] = node_data.date.dt.date

# Сохранение dgu_data и node_data в файл pickle
import pickle
with open('day 2/marginal.pickle', 'wb') as f:
    pickle.dump((dgu_data, node_data), f)

# Загрузка dgu_data и node_data из файла pickle
import pickle
with open('day 2/marginal.pickle', 'rb') as f:
    dgu_data, node_data = pickle.load(f)

x=1
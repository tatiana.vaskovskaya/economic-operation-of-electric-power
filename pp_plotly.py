# -*- coding: utf-8 -*-

# Copyright (c) 2016-2021 by University of Kassel and Fraunhofer Institute for Energy Economics
# and Energy System Technology (IEE), Kassel. All rights reserved.


import pandas as pd
import networkx as nx

from pandapower.plotting.generic_geodata import create_generic_coordinates
from pandapower.plotting.plotly.mapbox_plot import *
from pandapower.plotting.plotly.traces import draw_traces, version_check, \
    create_bus_trace, create_line_trace

from pandapower.pypower.idx_bus import BASE_KV
import pandapower as pp

from pypower.api import ppc_to_dataframes

from numpy import nan, isnan, arange, dtype, isin, any as np_any, zeros, array, bool_, \
    all as np_all, float64

try:
    import pplog as logging
except ImportError:
    import logging
logger = logging.getLogger(__name__)


def draw_ppc_result(ppc):
    net = from_ppc(ppc)
    draw_net_result(net)


def draw_net_result(net, cmap="Jet", use_line_geodata=None, on_map=False, projection=None,
                  map_style='basic', figsize=1, aspectratio='auto', line_width=2, bus_size=10,
                  climits_volt=(0.9, 1.1), climits_load=(0, 100), cpos_volt=1.0, cpos_load=1.1,
                  filename="plot.html"):
    """
    Plots a pandapower network in plotly
    using colormap for coloring lines according to line loading and buses according to voltage in p.u.
    If no geodata is available, artificial geodata is generated. For advanced plotting see the tutorial

    INPUT:
        **net** - The pandapower format network. If none is provided, mv_oberrhein() will be
        plotted as an example

    OPTIONAL:
        **respect_switches** (bool, False) - Respect switches when artificial geodata is created

        *cmap** (str, True) - name of the colormap

        *colors_dict** (dict, None) - by default 6 basic colors from default collor palette is used.
        Otherwise, user can define a dictionary in the form: voltage_kv : color

        **on_map** (bool, False) - enables using mapbox plot in plotly
        If provided geodata are not real geo-coordinates in lon/lat form, on_map will be set to False.

        **projection** (String, None) - defines a projection from which network geo-data will be transformed to
        lat-long. For each projection a string can be found at http://spatialreference.org/ref/epsg/

        **map_style** (str, 'basic') - enables using mapbox plot in plotly

            - 'streets'
            - 'bright'
            - 'light'
            - 'dark'
            - 'satellite'

        **figsize** (float, 1) - aspectratio is multiplied by it in order to get final image size

        **aspectratio** (tuple, 'auto') - when 'auto' it preserves original aspect ratio of the network geodata
        any custom aspectration can be given as a tuple, e.g. (1.2, 1)

        **line_width** (float, 1.0) - width of lines

        **bus_size** (float, 10.0) -  size of buses to plot.

        **climits_volt** (tuple, (0.9, 1.0)) - limits of the colorbar for voltage

        **climits_load** (tuple, (0, 100)) - limits of the colorbar for line_loading

        **cpos_volt** (float, 1.0) - position of the bus voltage colorbar

        **cpos_load** (float, 1.1) - position of the loading percent colorbar

        **filename** (str, "temp-plot.html") - filename / path to plot to. Should end on *.html

    OUTPUT:
        **figure** (graph_objs._figure.Figure) figure object

    """
    version_check()

    # create geocoord if none are available
    if 'line_geodata' not in net:
        net.line_geodata = pd.DataFrame(columns=['coords'])
    if 'bus_geodata' not in net:
        net.bus_geodata = pd.DataFrame(columns=["x", "y"])
    if len(net.line_geodata) == 0 and len(net.bus_geodata) == 0:
        logger.warning("No or insufficient geodata available --> Creating artificial coordinates." +
                       " This may take some time")
        create_generic_coordinates(net, respect_switches=True)
        if on_map:
            logger.warning("Map plots not available with artificial coordinates and will be disabled!")
            on_map = False
    for geo_type in ["bus_geodata", "line_geodata"]:
        dupl_geo_idx = pd.Series(net[geo_type].index)[pd.Series(
            net[geo_type].index).duplicated()]
        if len(dupl_geo_idx):
            if len(dupl_geo_idx) > 20:
                logger.warning("In net.%s are %i duplicated " % (geo_type, len(dupl_geo_idx)) +
                               "indices. That can cause troubles for draw_traces()")
            else:
                logger.warning("In net.%s are the following duplicated " % geo_type +
                               "indices. That can cause troubles for draw_traces(): " + str(
                    dupl_geo_idx))


    # check if geodata are real geographycal lat/lon coordinates using geopy
    if on_map and projection is not None:
        geo_data_to_latlong(net, projection=projection)

    # ----- Buses ------
    # initializating bus trace
    # hoverinfo which contains name and pf results
    precision = 3
    hoverinfo = (
            net.bus.name.astype(str) + '<br />' +
            # 'Pd = ' + net.bus.pd.round(precision).astype(str) + 'MW' + '<br />' +
            # 'Qd = ' + net.bus.qd.round(precision).astype(str) + 'MVAr' + '<br />' +
            # 'Pg = ' + net.bus.pg.round(precision).astype(str) + 'MW' + '<br />' +
            # 'Qg = ' + net.bus.qg.round(precision).astype(str) + 'MVAr' + '<br />' +
            'lam P = ' + net.bus.lam_p.round(precision).astype(str) + '$/MW' + '<br />' +
            'lam Q = ' + net.bus.lam_q.round(precision).astype(str) + '$/MVAr' + '<br />' +
            'V_m = ' + net.bus.vm.round(precision).astype(str) + ' pu' + '<br />' +
            'V_m = ' + (net.bus.vm * net.bus.vn_kv.round(2)).round(precision).astype(str) + ' kV' + '<br />' +
            'V_a = ' + net.bus.va.round(precision).astype(str) + ' deg').tolist()
    hoverinfo = pd.Series(index=net.bus.index, data=hoverinfo)
    cmin_lamp = net.bus.lam_p.min()
    cmax_lamp = net.bus.lam_p.max()
    bus_trace = create_bus_trace(net, net.bus.index, size=bus_size, infofunc=hoverinfo, cmap=cmap,
                                 cbar_title='Price, $/MW', cmin=cmin_lamp, cmax=cmax_lamp, cmap_vals=net.bus.lam_p,
                                 cpos=cpos_volt)

    # ----- Lines ------
    # if bus geodata is available, but no line geodata
    cmap_lines = 'jet' if cmap == 'Jet' else cmap
    if use_line_geodata is None:
        use_line_geodata = False if len(net.line_geodata) == 0 else True
    elif use_line_geodata and len(net.line_geodata) == 0:
        logger.warning("No or insufficient line geodata available --> only bus geodata will be used.")
        use_line_geodata = False
    # hoverinfo which contains name and pf results
    hoverinfo = (
            net.line.name.astype(str) + '<br />' +
            'P_from = ' + net.line.pf.round(precision).astype(str) + ' MW' + '<br />' +
            'P_to = ' + net.line.pt.round(precision).astype(str) + ' VW' + '<br />' +
            'Q_from = ' + net.line.qf.round(precision).astype(str) + ' MVAr' + '<br />' +
            'Q_to = ' + net.line.qt.round(precision).astype(str) + ' MVAr' + '<br />' +
            'sigma_from = ' + net.line.mu_sf.round(precision).astype(str) + ' MVAr' + '<br />' +
            'sigma_to = ' + net.line.mu_st.round(precision).astype(str) + ' MVAr' + '<br />' +
            'F_max = ' + net.line.rate_a.round(precision).astype(str) + ' MW' + '<br />' +
            'loading_percent = ' + net.line.loading_percent.round(precision).astype(str) + ' %').tolist()
    hoverinfo = pd.Series(index=net.line.index, data=hoverinfo)
    line_traces = create_line_trace(net, use_line_geodata=use_line_geodata, respect_switches=True,
                                    width=line_width,
                                    infofunc=hoverinfo,
                                    cmap=cmap_lines,
                                    cmap_vals=net.line['loading_percent'].values,
                                    cmin=climits_load[0],
                                    cmax=climits_load[1],
                                    cbar_title='Line Loading, %',
                                    cpos=cpos_load)

    return draw_traces(line_traces + bus_trace,
                       showlegend=False, aspectratio=aspectratio, on_map=on_map,
                       map_style=map_style, figsize=figsize, filename=filename)


def from_ppc(ppc):
    """
    This function converts pypower case files to pandapower net structure.

    INPUT:        **ppc** : The pypower case file.
    OUTPUT:        **net** : pandapower net.

    """
    # --- catch common failures
    if pd.Series(ppc['bus'][:, BASE_KV] <= 0).any():
        logger.info('There are false baseKV given in the pypower case file.')

    # --- general_parameters
    baseMVA = ppc['baseMVA']
    ppc_df = ppc_to_dataframes(ppc)

    net = pp.create_empty_network(f_hz=50, sn_mva=baseMVA)

    # --- bus data -> create buses, sgen, load, shunt
    # create buses
    net.bus = ppc_df.bus[['bus_i', 'vm', 'base_kv', 'va', 'vmax', 'vmin', 'lam_p', 'lam_q', 'bus_type']]\
        .rename(columns={'bus_i': 'name', 'base_kv': 'vn_kv', 'vmax': 'max_vm_pu', 'vmin': 'min_vm_pu'})
    net.bus['name'] = net.bus.name.astype(int)
    net.bus['in_service'] = net.bus.bus_type != 4

    net.res_bus = net.bus[['lam_p']]

    # TODO собрать pd, qd, pg, qg

    # --- branch data -> create line, trafo
    net.line = ppc_df.branch[['f_bus', 't_bus', 'rate_a', 'tap', 'shift', 'br_status',  'pf', 'qf', 'pt', 'qt',
                              'mu_sf', 'mu_st']].rename(columns={'f_bus': 'from_bus', 't_bus': 'to_bus',
                                                                 'br_status': 'in_service'})
    net.line['loading_percent'] = np.maximum(net.line.pf, net.line.pt) / net.line.rate_a * 100
    net.line['length_km'] = 1.
    net.line['name'] = net.line.from_bus.astype(int).astype(str) + '-' + net.line.to_bus.astype(int).astype(str)
    net.line[['from_bus', 'to_bus']] -= 1

    return net

def draw_ats_result(g, node_data, branch_data, cmap="Jet", projection=None,
                    map_style='basic', figsize=1, aspectratio='auto', line_width=2, bus_size=10,
                    cpos_volt=1.0, cpos_load=1.1, filename="plot.html"):

    net = pp.create_empty_network(f_hz=50, sn_mva=100.)

    # --- bus data -> create buses, sgen, load, shunt
    # create buses
    net.bus = node_data[['node_id', 'node_name', 'price', 'u']] \
        .rename(columns={'node_name': 'name', 'u': 'vn_kv', 'price': 'lam_p'})
    net.bus['in_service'] = net.bus.lam_p.notna()
    net.bus.set_index('node_id', inplace=True)

    net.res_bus = net.bus[['lam_p']]
    net.res_bus.index = net.bus.index

    # --- branch data -> create line
    net.line = branch_data[['node_from', 'node_to', 'flow']] \
        .rename(columns={'node_from': 'from_bus', 'node_to': 'to_bus'})
    net.line['length_km'] = 1.

    line_name_from = net.bus.loc[net.line.from_bus, 'name']
    line_name_to = net.bus.loc[net.line.to_bus, 'name']
    net.line['name'] = line_name_from.to_numpy() + '-' + line_name_to.to_numpy()

    # --- coords data -> create coords
    coords = nx.spring_layout(g)
    net.bus_geodata = pd.DataFrame({'x': [c[1] for k, c in coords.items()],
                                    'y': [c[0] for k, c in coords.items()]})
    net.bus_geodata.index = net.bus.index

    # ----- Buses ------
    # initializating bus trace
    # hoverinfo which contains name and pf results
    precision = 3
    hoverinfo = (
            net.bus.name.astype(str) + '<br />' +
            'lam P = ' + net.bus.lam_p.round(precision).astype(str) + 'rub./MWh' + '<br />' +
            'V_nom = ' + net.bus.vn_kv.round(precision).astype(str) + ' kV').tolist()
    hoverinfo = pd.Series(index=net.bus.index, data=hoverinfo)
    cmin_lamp = net.bus.lam_p.min()
    cmax_lamp = net.bus.lam_p.max()
    bus_trace = create_bus_trace(net, net.bus.index, size=bus_size, infofunc=hoverinfo, cmap=cmap,
                                 cbar_title='Price, rub./MWh', cmin=cmin_lamp, cmax=cmax_lamp, cmap_vals=net.bus.lam_p,
                                 cpos=cpos_volt)

    # ----- Lines ------
    # if bus geodata is available, but no line geodata
    cmap_lines = 'jet' if cmap == 'Jet' else cmap
    use_line_geodata = None
    # hoverinfo which contains name and pf results
    hoverinfo = (
            net.line.name.astype(str) + '<br />' +
            'Flow = ' + net.line.flow.round(precision).astype(str) + ' MW').tolist()
    hoverinfo = pd.Series(index=net.line.index, data=hoverinfo)
    cmin_flow = net.line.flow.abs().min()
    cmax_flow = net.line.flow.abs().max()
    line_traces = create_line_trace(net, use_line_geodata=use_line_geodata, respect_switches=True,
                                    width=line_width,
                                    infofunc=hoverinfo,
                                    cmap=cmap_lines,
                                    cmap_vals=net.line['flow'].to_numpy(),
                                    cmin=cmin_flow,
                                    cmax=cmax_flow,
                                    cbar_title='Flow, MW',
                                    cpos=cpos_load)

    return draw_traces(line_traces + bus_trace,
                       showlegend=False, aspectratio=aspectratio, on_map=False,
                       map_style=map_style, figsize=figsize, filename=filename)
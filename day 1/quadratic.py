# Импорт библиотек
from pypower.api import case30, runpf, runopf, ppoption

# Загрузка ЭЭС с 30 узлами
ppc = case30()

# Настройки параметров оптимизации
ppopt = ppoption(OPF_FLOW_LIM=1  # active power flow (limit in MW)
                 )

# Запуск расчета установившегося режима ЭЭС
ppc, success = runpf(casedata=ppc, ppopt=ppopt)

# Запуск оптимизации установившихся режимов ЭЭС
ppc = runopf(casedata=ppc, ppopt=ppopt)
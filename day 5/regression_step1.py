import loadutil.scrapping.ats_so as ats_so
from datetime import date, timedelta
import pandas as pd

date_to = date.today()
date_from = date.today() - timedelta(days=30)

# Загрузка данных по ветвям
r = ats_so.BranchReport()
r.verbose = True
branch_data = r.download(date_from, date_to)

# Загрузка данных по регионам
r = ats_so.RegionReport()
r.verbose = True
region_data = r.download(date_from, date_to)

# Сохраненине данных в файл
import pickle
with open('regression_day.pickle', 'wb') as f:
    pickle.dump((region_data, branch_data), f)

# # Загрузка данных из файла
# import pickle
# with open('regression_day.pickle', 'wb') as f:
#     region_data, branch_data = pickle.load(f)

# Посмотрим, какие регионы присутствуют
regions = region_data[['region_name', 'region_id']].drop_duplicates()

#ОЭС Центра: г. Москвы; +Белгородской, +Владимирской, +Вологодской, +Воронежской, +Ивановской, +Костромской, +Курской,
# +Орловской, +Липецкой, +Рязанской, +Брянской, +Калужской, +Смоленской, +Тамбовской, +Тверской, +Тульской, +Ярославской и +Московской области.
center_regions = [14, 15, 17, 19, 20, 24, 29, 34, 38, 42, 46, 54, 61, 66, 68, 28, 69, 78]
#ОЭС Средней Волги: +Пензенской, +Самарской, +Саратовской, +Ульяновской и +Нижегородской областей; +республик Чувашии,
# +Марий Эл, +Мордовии и +Татарстана.
volga_regions = [22, 56, 88, 89, 92, 36, 63, 73, 97]
#ОЭС Урала: +Республики Башкортостан и +Удмуртской Республики, Ханты-Мансийского и Ямало-Ненецкого автономных округов,
# +Пермского края, +Кировской, +Оренбургской, +Свердловской, +Курганской, Тюменской и +Челябинской областей.
# При этом +Тюменская энергосистема объединяет Тюменскую область, Ханты-Мансийский и Ямало-Ненецкий автономные округа.
ural_regions = [33, 37, 53, 57, 80, 65, 71, 94, 75]
# ОЭС Северо-Запада: г. Санкт-Петербурга, Мурманской, Калининградской, Ленинградской, Новгородской,
# Псковской и Архангельской областей, республик Карелия и Коми, Ненецкого
north_regions = [47, 41, 58, 86]

# Определим ОЭС
center_id = 5
volga_id = 2
ural_id = 1
north_id = 4
region_data['oes'] = 0
region_data.loc[region_data.region_id.isin(center_regions), 'oes'] = center_id
region_data.loc[region_data.region_id.isin(volga_regions), 'oes'] = volga_id
region_data.loc[region_data.region_id.isin(ural_regions), 'oes'] = ural_id
region_data.loc[region_data.region_id.isin(north_regions), 'oes'] = north_id

# Сгруппируем данные до ОЭС
region_data['amt_con'] = region_data.con_total * region_data.price_con
oes_data = region_data.groupby(by=['oes', 'date', 'hour'])[['con_total', 'amt_con']].sum().reset_index()
oes_data = oes_data[oes_data.oes.isin([1, 2, 4, 5])]
oes_data['price'] = oes_data.amt_con / oes_data.con_total

# Найдем перетоки между ОЭС по ветвям
# узлы 500000-599999 - центр, 200000-299999 - ср. Волга, 100000-199999
oes_flow_data_list = []
for oes_from, oes_to, dir in [(1, 2, -1), (2, 1, 1), (2, 5, -1), (5, 2, 1), (5, 4, 1), (4, 5, -1)]:
    current_flow_data = branch_data.loc[
        (branch_data.node_from >= oes_from * 100000) & (branch_data.node_from <= oes_from * 100000 + 99999) &
        (branch_data.node_to >= oes_to * 100000) & (branch_data.node_to <= oes_to * 100000 + 99999),
        ['date', 'hour', 'flow']
    ]
    current_flow_data['flow'] *= dir
    if dir == 1:
        current_flow_data['oes_from'] = oes_from
        current_flow_data['oes_to'] = oes_to
    else:
        current_flow_data['oes_from'] = oes_to
        current_flow_data['oes_to'] = oes_from

    oes_flow_data_list.append(current_flow_data)

oes_flow_data = pd.concat(oes_flow_data_list).groupby(by=['date', 'hour', 'oes_from', 'oes_to']).sum().reset_index()

# Сохранение результата в файл
import pickle
with open('regression.pickle', 'wb') as f:
    pickle.dump((oes_data, oes_flow_data), f)

x = 1

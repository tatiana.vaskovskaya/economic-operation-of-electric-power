clc, clear

%��������� ����� �� 30 �����
mpc = loadcase('case30');

%������������ �� ��� ������������ ���������
mpopt = mpoption('model', 'DC', 'opf.flow_lim', 'P'); %� ������������ MOST ��������, ��� ���� ���������� DC OPF, AC OPF ����� � ��������� �������
mpc = runopf(mpc);

%��������� ���. ���������� �� �����������
xgd = loadxgendata('xgd_case30', mpc);

%������� ���������� �� �������� � ex_storage
storage = storage_case30;

%��������� ���������� � ����
[iess, storage_mpc, xgd, sd] = addstorage(storage, mpc, xgd);

%��������� �������
profiles = getprofiles('profile_case30', iess);

%������� Multi-period case
nt = 24;
mdi = loadmd(storage_mpc, nt, xgd, sd, [], profiles);

%��������� �����������
mdo = most(mdi, mpopt);

%���������� ����������
figure
plot(mdo.results.GenPrices(7,:))
figure
plot(mdo.results.ExpectedDispatch(7,:))
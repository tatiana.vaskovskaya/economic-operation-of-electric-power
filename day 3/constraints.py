from pypower.api import case30, runpf, runopf, ppoption, ppc_to_dataframes, dataframes_to_ppc
import numpy as np
from pp_plotly import draw_ppc_result

ppc = case30()
ppopt = ppoption(OPF_FLOW_LIM=1  # active power flow (limit in MW)
                 )
ppc, success = runpf(casedata=ppc, ppopt=ppopt)
ppc_df = ppc_to_dataframes(ppc)

# формируем линейные заявки
# согласно мануалу matpower https://matpower.org/docs/MATPOWER-manual-7.1.pdf
# gencost собран следующим образом
# column 1 - Model - 1 = piecewise linear, 2 = polynomial
# column 2 - Startup - not used
# column 3 - Shutdown - not used
# column 4 - Ncost - Number of data points defining n-segment piecewise linear functions,
#                                                   or coefficients of polynomial cost
# column 5 - Cost - (Model=1) p1,f1,p2,f2... p - power, f - cost - connected points of cost function
#                   (Model=2) cn,...,c1,c0 for f(p) = cn*p^n + ... c1*p + c0

ppc_df.gencost['model'] = ppc_df.gencost_model.polynomial
ppc_df.gencost['ncost'] = 2  # два коэффициента cost = col1 * P + col2
ppc_df.gencost.drop(columns='col3', inplace=True)
ppc_df.gencost['col1'] = np.arange(1000., 1600., 100.)  # цены в заявке
ppc_df.gencost['col2'] = 0.  # нет постоянных расходов

# Что произойдет, если мы ограничим переток по какой-нибудь линии?
ppc_df.branch.loc[(ppc_df.branch.f_bus == 6) & (ppc_df.branch.t_bus == 8), 'rate_a'] = 25.0
# а еще ?
ppc_df.branch.loc[(ppc_df.branch.f_bus == 2) & (ppc_df.branch.t_bus == 4), 'rate_a'] = 30.0

# Что произойдет, если мы ограничим напряжение в каком-нибудь узле?
ppc_df.bus.loc[ppc_df.bus.bus_i == 13, 'vmax'] = 1.05


ppc = dataframes_to_ppc(ppc_df)

# Оптимизируем
solved_ppc = runopf(casedata=ppc, ppopt=ppopt)

# Выводим результат на схему
draw_ppc_result(solved_ppc)



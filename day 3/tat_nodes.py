import loadutil.scrapping.ats_so as ats_so
from datetime import date
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import matplotlib.pyplot as plt
from pp_plotly import draw_ats_result

date_to = date.today()
date_from = date.today()

# # Загрузка данных по узлам
# r = ats_so.NodePriceReport()
# r.verbose = True
# node_data = r.download(date_from, date_to)
# node_data['date'] = node_data.date.dt.date
#
# # Загрузка данных по ветвиям
# r = ats_so.BranchReport()
# r.verbose = True
# branch_data = r.download(date_from, date_to).drop_duplicates()
# branch_data['date'] = branch_data.date.dt.date

import pickle
with open('day 3/tatnodes.pickle', 'rb') as f:
    node_data, branch_data = pickle.load(f)

# Фильтр по узлам Татарстана
node_data = node_data[node_data.region_id == 92]
tat_nodes = node_data.node_id.unique()
branch_data = branch_data[(branch_data.node_from.isin(tat_nodes)) & (branch_data.node_to.isin(tat_nodes))]
branch_data = branch_data.groupby(by=['date', 'hour', 'node_from', 'node_to'])['flow'].sum().reset_index()

# Отрисовка схемы
hour = 10
# фильтр по часу
nodes = node_data.loc[node_data.hour == hour, ['node_id', 'node_name', 'price', 'u']]
edges = branch_data.loc[branch_data.hour == hour, ['node_from', 'node_to', 'flow']]

# Создаем экземпляр класса networkx Graph
graph = nx.Graph()
# Добавляем в граф узлы из списка узлов
graph.add_nodes_from(nodes.node_id)
# Добавляем в граф ветви список из (узел начала, узел конца)
graph.add_edges_from(list(edges[['node_from', 'node_to']].itertuples(index=False, name=None)))
# Отобразим граф с помощью встроенной возможности работы с matplotlib
nx.draw(graph, pos=nx.spring_layout(graph), with_labels=True)
plt.show()

# Сделаем что-то более красивое с plotly
draw_ats_result(graph, nodes, edges)


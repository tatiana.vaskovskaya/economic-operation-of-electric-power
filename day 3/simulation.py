from pypower.api import case30, runpf, runopf, ppoption, ppc_to_dataframes, dataframes_to_ppc
import pandas as pd

#изначальные предельне затраты (полезность)
COSTS = [1350, 1700, 1100, 1650, 1250, 1200] + \
        [1500, 1850, 2050, 1550, 2250, 1750, 2000, 1650, 1950, 2300, 2100, 2150, 2450, 2350, 2200, 2400, 1800, 1700, 1900, 1600]

ppc = case30()
ppopt = ppoption(OPF_FLOW_LIM=1  # active power flow (limit in MW)
                 )
ppc, success = runpf(casedata=ppc, ppopt=ppopt)
ppc_df = ppc_to_dataframes(ppc) # удобнее работать с датафреймами, а не матрицами

## Чтобы добавить заявки потребителей - их надо сделать генераторами с отрицательной мощностью
## Собираем gen table
# Фильтр для всех потребителей, у которых есть нагрузка
load_filter = ppc_df.bus.pd > 0.0
# данные об управляемых нагрузках, которые добавим в таблицу генераторов
dispatchable_load = pd.DataFrame({
    'gen_bus': ppc_df.bus.bus_i[load_filter],
    'pg': - ppc_df.bus.pd[load_filter],
    'vg': ppc_df.bus.vm[load_filter],
    'pmin': - ppc_df.bus.pd[load_filter],
})
dispatchable_load['gen_status'] = 1
dispatchable_load['mbase'] = 100.

# Имя нагрузки для отображения: load + номер узла
dispatchable_load['name'] = 'load' + dispatchable_load.gen_bus.astype(str)

# Имя генератора для отображения: gen + номер узла
ppc_df.gen['name'] = 'gen' + ppc_df.gen.gen_bus.astype(str)

# объединяем в одну таблицу
ppc_df.gen = pd.concat((ppc_df.gen, dispatchable_load))
# все пустые значения заполняем нулями
ppc_df.gen.fillna(0.0, inplace=True)
ppc_df.gen = ppc_df.gen.reset_index(drop=True)

# Собираем gencost table
# Делаем заявки линейными
ppc_df.gencost['model'] = ppc_df.gencost_model.polynomial
ppc_df.gencost['ncost'] = 2 # два коэффициента cost = col1 * P + col2
ppc_df.gencost.drop(columns='col3', inplace=True)
ppc_df.gencost['col1'] = 1000. # далее переопределим
ppc_df.gencost['col2'] = 0.

# Часть для управляемых нагрузок
nd = dispatchable_load.shape[0]  # Количество управляемых нагрузок
dispatchable_load_cost = pd.DataFrame({
    'model': [ppc_df.gencost_model.polynomial] * nd,
    'ncost': [2] * nd,
    'col1': [10000] * nd,
})
# объединяем в одну таблицу
ppc_df.gencost = pd.concat((ppc_df.gencost, dispatchable_load_cost))
# все пустые значения заполняем нулями
ppc_df.gencost.fillna(0.0, inplace=True)
ppc_df.gencost = ppc_df.gencost.reset_index(drop=True)

# Добавляем имя
ppc_df.gencost['name'] = ppc_df.gen.name



# Убираем нагрузки из таблицы узлов
ppc_df.bus['pd'] = 0.0
# ppc_df.bus['qd'] = 0.0
# ppc_df.bus['bs'] = qd / ppc_df.bus.vm ** 2 / ppc_df.bus.base_kv ** 2

# Чтобы было легко ставить заявки
ppc_df.gencost.set_index('name', inplace=True)
ppc_df.gen.set_index('name', inplace=True)

# Изначально все заявки равны предельным затратам (полезности)
ppc_df.gencost['col1'] = COSTS
# Здесь меняем на поданные игроками заявки (лишние можно удалить, либо повторить значения из COSTS)
ppc_df.gencost.loc['gen1', 'col1'] = 1350
ppc_df.gencost.loc['gen2', 'col1'] = 1700
ppc_df.gencost.loc['gen22', 'col1'] = 1100
ppc_df.gencost.loc['gen27', 'col1'] = 1650
ppc_df.gencost.loc['gen23', 'col1'] = 1250
ppc_df.gencost.loc['gen13', 'col1'] = 1200
ppc_df.gencost.loc['load2', 'col1'] = 1500
ppc_df.gencost.loc['load3', 'col1'] = 1850
ppc_df.gencost.loc['load4', 'col1'] = 2050
ppc_df.gencost.loc['load7', 'col1'] = 1550
ppc_df.gencost.loc['load8', 'col1'] = 2250
ppc_df.gencost.loc['load10', 'col1'] = 1750
ppc_df.gencost.loc['load12', 'col1'] = 2000
ppc_df.gencost.loc['load14', 'col1'] = 1650
ppc_df.gencost.loc['load15', 'col1'] = 1950
ppc_df.gencost.loc['load16', 'col1'] = 2300
ppc_df.gencost.loc['load17', 'col1'] = 2100
ppc_df.gencost.loc['load18', 'col1'] = 2150
ppc_df.gencost.loc['load19', 'col1'] = 2450
ppc_df.gencost.loc['load20', 'col1'] = 2350
ppc_df.gencost.loc['load21', 'col1'] = 2200
ppc_df.gencost.loc['load23', 'col1'] = 2400
ppc_df.gencost.loc['load24', 'col1'] = 1800
ppc_df.gencost.loc['load26', 'col1'] = 1700
ppc_df.gencost.loc['load29', 'col1'] = 1900
ppc_df.gencost.loc['load30', 'col1'] = 1600

ppc_df.gencost.reset_index(inplace=True)
ppc_df.gen.reset_index(inplace=True)

ppc_df.bus[['pd', 'qd']] *= 1.0 # коэффициент изменения нагрузки

# возвращаемся обратно к матрицам
ppc = dataframes_to_ppc(ppc_df)
# оптимизируем
ppc = runopf(casedata=ppc, ppopt=ppopt)
# снова переводим в датафреймы, чтобы анализировать результат
ppc_df = ppc_to_dataframes(ppc)

# расчет выигрыша
revenue = pd.DataFrame({'name': ppc_df.gen.name, 'cost': COSTS, 'dir':[1]*6+[-1]*20})

ppc_df.bus.set_index('bus_i', inplace=True)
revenue['price'] = ppc_df.bus.loc[ppc_df.gen.gen_bus, 'lam_p'].reset_index(drop=True)
revenue['volume'] = ppc_df.gen.pg * revenue.dir
revenue['revenue'] = ppc_df.gen.pg * (revenue.price - revenue.cost)
revenue['name'] = ppc_df.gen.name
# Удалим лишнюю информацию
revenue.drop(columns=['cost', 'dir'], inplace=True)

x=1